package org.example.cep;

import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import static io.restassured.RestAssured.given;

public class InformeCep {

    public static ValidatableResponse postPesquisaCep(String cep) {

        return given()
                .contentType(ContentType.JSON)
                .relaxedHTTPSValidation()
                .when()
                .get("https://viacep.com.br/ws/" + cep + "/json")
                .then();
    }

}
