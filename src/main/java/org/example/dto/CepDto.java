package org.example.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class CepDto {
    private String cep;
    private String logradouro;
    private String complemento;
    private String bairro;
    private String localidade;
    private String uf;
    private String ibge;
    private String gia;
    private String ddd;
    private String siafi;

    @Override
    public String toString() {
        return "Informações do CEP{\n" +
                " cep= " + cep + '\n' +
                " logradouro= " + logradouro + '\n' +
                " complemento= " + complemento + '\n' +
                " bairro= " + bairro + '\n' +
                " localidade= " + localidade + '\n' +
                " uf= " + uf + '\n' +
                " ibge= " + ibge + '\n' +
                " gia= " + gia + '\n' +
                " ddd= " + ddd + '\n' +
                " siafi= " + siafi + '\n' +
                '}';
    }
}
