package org.example.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class CepDtoInvalido {
    private String erro;

    @Override
    public String toString() {
        return "CepDtoInvalido{\n" +
                "erro= " + erro + '\n' +
                '}';
    }
}
