package buscaCep;

import org.example.dto.CepDto;
import org.example.dto.CepDtoInvalido;
import org.junit.Test;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.example.cep.InformeCep.postPesquisaCep;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ConsultaCep {

    @Test
    public void validaCpfValido() {

        CepDto cepConsulta = postPesquisaCep("90640030")
                .statusCode(200)
                .body(matchesJsonSchemaInClasspath("json_schemas/contrato_cep.json"))
                .extract().as(CepDto.class);
        System.out.println(cepConsulta);

        assertThat(cepConsulta, notNullValue());

    }

    @Test
    public void validaCpfInvalido() {

        CepDtoInvalido cepInvalido = postPesquisaCep("10020666")
                .statusCode(200)
                .body("erro", is(true))
                .extract().as(CepDtoInvalido.class);
        System.out.println(cepInvalido);

    }
}